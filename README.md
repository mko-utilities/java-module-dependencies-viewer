# Java Module Dependencies Viewer

This project contains a small python script allowing to visualize dependencies defined in a set of java-module.info files.

This project is distributed on [PyPI](https://pypi.org) on the following page: https://pypi.org/project/java-module-dependencies-viewer/

## Alternatives solutions

This script is way simpler than the following other solutions:
- https://livebook.manning.com/book/the-java-9-module-system/d-analyzing-a-project-s-dependencies-with-jdeps/v-9/11
- https://nipafx.dev/jdeps-tutorial-analyze-java-project-dependencies/
- https://github.com/cartermc24/Java-Dependency-Visualizer
- https://github.com/rahmanusta/module-graph
- https://github.com/ExpediaGroup/jarviz

## Build and distribution process

Distribution of this script used information available at:
* https://packaging.python.org/en/latest/tutorials/packaging-projects/
* https://scikit-hep.org/developer/pep621
and the following exemple https://github.com/kubernetes-client/python.
