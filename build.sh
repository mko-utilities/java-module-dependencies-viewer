#!/bin/bash

echo -e "\n === Updating the build system ===" & sleep 1s;
python3 -m pip install --upgrade build || exit 1;

echo -e "\n === Building the project ===" & sleep 1s;
python3 -m build || exit 2;

echo -e "\n === Updating Twine ===" & sleep 1s;
python3 -m pip install --upgrade twine || exit 3;

echo -e "\n === Uploading the project to PyPI ===" & sleep 1s;
python3 -m twine upload dist/* || exit 4;
